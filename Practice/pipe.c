#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


void basic_pipe_communication(){
	int fd[2];
    pipe(fd);
    printf("Reading from %d, writing to %d\n", fd[0], fd[1]);

    pid_t p = fork();
    if (p > 0) {
        write(fd[1],"Hi Child!\n",9);
        wait(NULL);
    } else {
        char buf;
        int bytesread;
        while ((bytesread = read(fd[0], &buf, 1)) > 0) {
            putchar(buf);
			fflush(stdout);
        }
    }
}


void reader_and_writer(){
	int fh[2];
    pipe(fh);
    FILE *reader = fdopen(fh[0], "r");
    FILE *writer = fdopen(fh[1], "w");
    pid_t p = fork();
    if (p > 0) {
        int score;
        fscanf(reader, "Score %d", &score);
        printf("The child says the score is %d\n", score);
    } else {
        fprintf(writer, "Score %d\n", 10 + 10);
        fflush(writer);
    }
    return 0;
}