#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>


void like_father_like_son(){
    int answer = 84 >> 1;
    printf("Answer: %d", answer);
    fork();
}


void father_first(){
    pid_t id = fork();
    if (id == -1) exit(1);
    if (id > 0){
        printf("I'm the father!\n");
        printf("%d\n", id);
    } else {
        printf("I'm the child!\n");
        printf("%d\n", id);
    }
}


void father_waiting_son(){
    pid_t child_id = fork();
    if (child_id == -1){
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (child_id > 0){
        int status;
        waitpid(child_id, &status, 0);
        printf("I'm the father, having id: %d!\n", getpid());
    } else {
        printf("I'm the child, having id: %d, while my father has id: %d!\n", getpid(), getppid());
        exit(123);
	}
}


void child_exec(int argc, char**argv){
	pid_t child = fork();
	if(child == -1){
		return EXIT_FAILURE;
	}
	if(child){
		printf("I'm a father! I have a child!!\n");
		int status;
		waitpid(child, &status, 0);
		return EXIT_SUCCESS;
	} else{
		execl("/bin/ls", "ls", "-alh", (char *) NULL);
		perror("exec failed!");
	}
}


void sleepsort(int argc, char**argv){
	while (--argc > 1 && !fork());
	int val = atoi(argv[argc]);
	sleep(val);
	printf("%d\n", val);
	return 0;
}
