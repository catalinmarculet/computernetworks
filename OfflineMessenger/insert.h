#include <sqlite3.h>
#include <stdio.h>
#include "select.h"

int insert_user(char *username, char *password)
{
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }
    else
    {
        fprintf(stdout, "Database Opened successfully\n");
    }

    sqlite3_stmt *stmt;
    char *sql_query = "INSERT INTO USERS (Username, Password) VALUES (?1, ?2);";

    sqlite3_prepare_v2(db, sql_query, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, username, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, password, -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_DONE)
    {
        fprintf(stdout, "ERROR inserting data: %s\n", sqlite3_errmsg(db));
    }
    else
    {
        fprintf(stdout, "User %s added\n", username);
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
}

int select_user_id(sqlite3 *db, int rc, char *username)
{
    sqlite3_stmt *stmt;
    int id;
    char *select_id_query = "SELECT Id FROM USERS WHERE Username = ?1";

    sqlite3_prepare_v2(db, select_id_query, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, username, -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_ROW)
    {
        fprintf(stdout, "ERROR receiving data: %s\n", sqlite3_errmsg(db));
    }
    else
    {
        id = sqlite3_column_int(stmt, 0);
    }

    return id;
}

int insert_message_using_names(char *sender, char *receiver, char *message)
{
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }
    else
    {
        fprintf(stdout, "Database Opened successfully\n");
    }

    sqlite3_stmt *stmt;
    int id_sender = select_user_id(db, rc, sender);
    int id_receiver = select_user_id(db, rc, receiver);
    printf("The sender is: %s... and has the id %d\n", sender, id_sender);
    printf("The receiver is: %s... and has the id %d\n", receiver, id_receiver);
    char *insert_query = "INSERT INTO MESSAGES (Sender, Receiver, Read, Message) VALUES (?1, ?2, 0, ?3);";

    sqlite3_prepare_v2(db, insert_query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, id_sender);
    sqlite3_bind_int(stmt, 2, id_receiver);
    sqlite3_bind_text(stmt, 3, message, -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_DONE)
    {
        fprintf(stdout, "ERROR inserting data: %s\n", sqlite3_errmsg(db));
    }
    else
    {
        fprintf(stdout, "The message is: \"%s\" and it was added into the database\n", message);
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
}

int insert_message(int id_sender, int id_receiver, char *message)
{
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }
    else
    {
        fprintf(stdout, "Database Opened successfully\n");
    }

    sqlite3_stmt *stmt; 
    char *insert_query = "INSERT INTO MESSAGES (Sender, Receiver, Read, Message) VALUES (?1, ?2, 0, ?3);";

    sqlite3_prepare_v2(db, insert_query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, id_sender);
    sqlite3_bind_int(stmt, 2, id_receiver);
    sqlite3_bind_text(stmt, 3, message, -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_DONE)
    {
        fprintf(stdout, "ERROR inserting data: %s\n", sqlite3_errmsg(db));
    }
    else
    {
        fprintf(stdout, "The message is: \"%s\" and it was added into the database\n", message);
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
}


int get_message_id(sqlite3 *db, int rc, char *message){
    sqlite3_stmt *stmt;
    int id;
    char *select_id_query = "SELECT Id FROM Messages WHERE Message = ?1";

    sqlite3_prepare_v2(db, select_id_query, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, message, -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_ROW)
    {
        fprintf(stdout, "ERROR receiving data: %s\n", sqlite3_errmsg(db));
    }
    else
    {
        id = sqlite3_column_int(stmt, 0);
    }

    return id;
}

int insert_into_replies(char* message, int message_id){
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }
    else
    {
        fprintf(stdout, "Database Opened successfully\n");
    }

    sqlite3_stmt *stmt;
    char* query;

    // Get message data
    query = "SELECT sender, receiver FROM messages WHERE id = ?1";
    sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, message_id);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_ROW) {
        printf("Error getting message info\n");
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        return -1;
    }
    int receiver_id = sqlite3_column_int(stmt, 0);
    int sender_id = sqlite3_column_int(stmt, 1);
    sqlite3_finalize(stmt);

    // Insert new message
    query = "INSERT INTO MESSAGES (Sender, Receiver, Read, Message) VALUES (?1, ?2, 0, ?3);";
    sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, sender_id);
    sqlite3_bind_int(stmt, 2, receiver_id);
    sqlite3_bind_text(stmt, 3, message, -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        printf("Error inserting new message to db\n");
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        return -1;
    }
    sqlite3_finalize(stmt);

    // Get id of inserted message
    query = "SELECT last_insert_rowid();";
    sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_ROW) {
        printf("Error inserting new message to db\n");
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        return -1;
    }
    int new_message_id = sqlite3_column_int(stmt, 0);
    sqlite3_finalize(stmt);

    // Insert into replies
    query = "INSERT INTO REPLIES (Parent, Child) VALUES (?1, ?2);";
    sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, message_id);
    sqlite3_bind_int(stmt, 2, new_message_id);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        printf("Error inserting reply to db\n");
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        return -1;
    }
    sqlite3_finalize(stmt);
    
    sqlite3_close(db);
}


// int main()
// {
//     insert_user("Catalin", "parola");
//     insert_user("Dan", "parola");
//     insert_user("MosCraciun", "parola");
//     printf("------------------------------------------- \n");

//     insert_message("MosCraciun", "Dan", "Hohohooo, Merry Christmas!");
//     insert_message("Dan", "MosCraciun", "Mersi man!");
//     insert_into_replies("Hohohooo, Merry Christmas!", "Mersi man!");
//     return 0;
// }