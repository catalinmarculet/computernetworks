#include <sqlite3.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

int mark_read(int receiver_id)
{
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }

    sqlite3_stmt *stmt;
    int id;
    printf("%d\n", receiver_id);
    char *query = "update messages set Read = 1 where Receiver = ?1";

    sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, receiver_id);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        printf("Couldn't mark as read\n");
    }
    sqlite3_close(db);
    return 0;
}
