#include <sqlite3.h>
#include <stdio.h>

int main(void)
{

    sqlite3 *db;
    char *err_message = 0;

    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }
    else
    {
        fprintf(stdout, "Database Opened successfully\n");
    }

    char *sql_query = "DROP TABLE IF EXISTS USERS;"
                      "DROP TABLE IF EXISTS MESSAGES;"
                      "DROP TABLE IF EXISTS REPLIES;"
                      "CREATE TABLE USERS(\
                            Id INTEGER PRIMARY KEY AUTOINCREMENT,\
                            Username TEXT,\\ 
                            Password TEXT)\
                        ;"
                      "CREATE TABLE MESSAGES(\
                            Id INTEGER PRIMARY KEY AUTOINCREMENT,\
                            Sender INTEGER,\\ 
                            Receiver INTEGER,\\ 
                            Read INTEGER,\\ 
                            Message TEXT)\
                        ;"
                      "CREATE TABLE REPLIES(\
                            Id INTEGER PRIMARY KEY AUTOINCREMENT,\
                            Parent INTEGER,\
                            Child INTEGER)\
                        ;";

    rc = sqlite3_exec(db, sql_query, 0, 0, &err_message);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", err_message);
        sqlite3_free(err_message);
        sqlite3_close(db);
        return 1;
    }
    else
    {
        fprintf(stdout, "Tables created successfully\n");
    }

    sqlite3_close(db);

    return 0;
}