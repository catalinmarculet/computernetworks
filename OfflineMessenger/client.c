/* cliTCP.c - Exemplu de client TCP 
   Trimite un nume la server; primeste de la server "Hello nume".
         
   Autor: Lenuta Alboaie  <adria@infoiasi.ro> (c)2009

*/
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <malloc.h>
#include <sqlite3.h>
#include "my_utils.h"

#define DISPlAY_UNREAD 10
#define SEND_NEW 11
#define VIEW_HISTORY 12
#define REPLY 13
#define MARK_READ 14

#define clear() printf("\033[H\033[J")

/* codul de eroare returnat de anumite apeluri */
extern int errno;

/* portul de conectare la server*/
int port;

int main(int argc, char *argv[])
{
    int fd;                    // descriptorul de socket
    struct sockaddr_in server; // structura folosita pentru conectare
    char username[100];
    char password[100];
    char* response;

    /* exista toate argumentele in linia de comanda? */
    if (argc != 3)
    {
        printf("[client] Sintaxa: %s <adresa_server> <port>\n", argv[0]);
        return -1;
    }

    /* stabilim portul */
    port = atoi(argv[2]);

    /* cream socketul */
    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("[client] Eroare la socket().\n");
        return errno;
    }

    /* umplem structura folosita pentru realizarea conexiunii cu serverul */
    /* familia socket-ului */
    server.sin_family = AF_INET;
    /* adresa IP a serverului */
    server.sin_addr.s_addr = inet_addr(argv[1]);
    /* portul de conectare */
    server.sin_port = htons(port);

    /* ne conectam la server */
    if (connect(fd, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1)
    {
        perror("[client]Eroare la connect().\n");
        return errno;
    }
    
    fflush(stdout);
    clear();
    printf("Welcome to Offline Messenger\n");

    printf("Please type your username and password in order to log in.\n\n");
    while(1){
        //username input
        do {
            printf("Username: ");
            fflush(stdout);
            fgets(username, 100, stdin);
            username[strlen(username)-1] = '\0';
        } while (strlen(username) == 0);

        //Send username to server
        if (write_string(fd, username) <= 0)
        {
            perror("[client]Eroare la write() spre server.\n");
            return errno;
        }

        //password input
        printf("Password: ");
        fflush(stdout);
        fgets(password, 100, stdin);
        password[strlen(password)-1] = '\0';

        //Send password to server
        if (write_string(fd, password) <= 0)
        {
            perror("[client]Eroare la write() spre server.\n");
            return errno;
        }

        //Read server login response
        int logged_in = -1;
        read_integer(fd, logged_in);
        //Read server login response
        if (logged_in < 0)
        {
            printf("Unverified credentials. Please try again.\n");
            continue;
        }
    
        printf("You logged in successfully!\n\n");
        break;
    /* inchidem conexiunea, am terminat */
    }

    write_integer(fd, DISPlAY_UNREAD);
    int nr_messages;
    read_integer(fd, nr_messages);
    int* messages_ids = (int*) malloc(nr_messages * sizeof(int));
    for (int i = 0; i < nr_messages; i++)
        read_integer(fd, messages_ids[i]);
    char* messages_view;
    read_string(fd, messages_view);

    if (nr_messages) {
        printf("You have %d unread mesasages. Press (d) to see them.\n", nr_messages);
    }
    else {
        printf("You have no unread messages. You can proceed to socializing with your firends.\n");
    }

    int should_show_help = true;
    char user_input[1000];
    while(1) {
        if (should_show_help) {
            // printf("\n");
            printf("Choose one of the following commands:\n");
            printf("(d) display unread messages (s) send new message (h) view message history\n");
        }
        printf(">>");
        fflush(stdout);

        fgets(user_input, 1000, stdin);
        user_input[strlen(user_input) - 1] = '\0';
        if (!strlen(user_input)) {
            should_show_help = false;
            continue;
        }

        should_show_help = true;
        if (!strcmp(user_input, "d")) {
            write_integer(fd, DISPlAY_UNREAD);
            read_integer(fd, nr_messages);
            messages_ids = (int*) malloc(nr_messages * sizeof(int));
            for (int i = 0; i < nr_messages; i++)
                read_integer(fd, messages_ids[i]);
            read_string(fd, messages_view);

            if (!nr_messages) {
                printf("You have no unread messages.\n");
                continue;
            }

            printf("You have %d unread messages:\n", nr_messages);
            printf("%s", messages_view);
            printf("You can select a [number] if you wish to reply to that message: ");
            fflush(stdout);

            write_integer(fd, MARK_READ);

            fgets(user_input, 1000, stdin);
            user_input[strlen(user_input) - 1] = '\0';
            int nr = atoi(user_input);
            if (nr <= 0)
                continue;

            nr -= 1;
            printf("Please write your message:\n");
            printf("--------------------------\n");
            fgets(user_input, 1000, stdin);
            user_input[strlen(user_input) - 1] = '\0';
            printf("--------------------------\n");

            int message_id = messages_ids[nr];
            write_integer(fd, REPLY);
            write_integer(fd, message_id);
            write_string(fd, user_input);
        }
        else if (!strcmp(user_input, "h")) {
            int page = 0;
            int in_history = true;
            while (in_history) {
                write_integer(fd, VIEW_HISTORY);
                write_integer(fd, page);
                
                read_integer(fd, nr_messages);
                messages_ids = (int*) malloc(nr_messages * sizeof(int));
                for (int i = 0; i < nr_messages; i++)
                    read_integer(fd, messages_ids[i]);
                read_string(fd, messages_view);

                clear();
                if (!nr_messages) {
                    printf("You have no more messages.\n");
                    in_history = false;
                    continue;
                }

                printf("Your last %d to %d messages are:\n", page + 1, page + 10);
                printf("%s", messages_view);
                printf("[number] give reply (n) next page (p) previous page\n");

                fgets(user_input, 1000, stdin);
                user_input[strlen(user_input) - 1] = '\0';
                int nr = atoi(user_input);
                if (nr > 0) {
                    nr -= 1;
                    printf("Please write your message:\n");
                    printf("--------------------------\n");
                    fgets(user_input, 1000, stdin);
                    user_input[strlen(user_input) - 1] = '\0';
                    printf("--------------------------\n");

                    int message_id = messages_ids[nr];
                    write_integer(fd, REPLY);
                    write_integer(fd, message_id);
                    write_string(fd, user_input);
                }
                else if (!strcmp(user_input, "n"))
                    page += 10;
                else if (!strcmp(user_input, "p")) {
                    page -= 10;
                    if (page < 0)
                        page = 0;
                }
                else
                    break;
            }
        }
        else if (!strcmp(user_input, "s")) {
            write_integer(fd, SEND_NEW);
            printf("To whom would you like to send your message? ");
            fflush(stdout);
            fgets(user_input, 1000, stdin);
            user_input[strlen(user_input) - 1] = '\0';
            write_string(fd, user_input);
            int status;
            read_integer(fd, status);
            if (status == -1) {
                printf("That username does not exist.\n");
                continue;
            }

            printf("Please write your message:\n");
            printf("--------------------------\n");
            fgets(user_input, 1000, stdin);
            user_input[strlen(user_input) - 1] = '\0';
            printf("--------------------------\n");
            write_string(fd, user_input);

        }
    }
    close(fd);
}
