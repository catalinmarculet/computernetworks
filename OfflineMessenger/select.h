#pragma once
#include <sqlite3.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

int select_user_id(char *username)
{
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }

    sqlite3_stmt *stmt;
    int id;
    char *select_id_query = "SELECT Id FROM USERS WHERE Username = ?1";

    sqlite3_prepare_v2(db, select_id_query, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, username, -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_ROW)
    {
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        return -1;
    }
    else
    {
        id = sqlite3_column_int(stmt, 0);
    }
    sqlite3_finalize(stmt);
    sqlite3_close(db);
    return id;
}

char* select_username(int id)
{
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
    }

    sqlite3_stmt *stmt;
    char* sql_query = "SELECT Username FROM USERS WHERE Id = ?1;";
    char* uname = (char*)malloc(200);
    // char** unames = malloc(20 * sizeof(char*));

    sqlite3_prepare_v2(db, sql_query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, id);
    rc = sqlite3_step(stmt);

    int i = 0;
    while (rc == SQLITE_ROW)
    {
        // unames[i] = malloc(200);
        strcpy(uname, (char*)sqlite3_column_text(stmt, 0));
        rc = sqlite3_step(stmt);
        ++i;
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
    return uname;
}

char* select_password(int id)
{
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
    }

    sqlite3_stmt *stmt;
    char* sql_query = "SELECT Password FROM USERS WHERE Id = ?1;";
    char* uname = (char*)malloc(200);
    // char** unames = malloc(20 * sizeof(char*));

    sqlite3_prepare_v2(db, sql_query, -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, id);
    rc = sqlite3_step(stmt);

    int i = 0;
    while (rc == SQLITE_ROW)
    {
        // unames[i] = malloc(200);
        strcpy(uname, (char*)sqlite3_column_text(stmt, 0));
        rc = sqlite3_step(stmt);
        ++i;
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
    return uname;
}

int select_messages(int id_user, int* &ids, char* &view, int page){
    sqlite3 *db;
    char *err_message = 0;
    int rc = sqlite3_open("mydb.db", &db);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Can not open the database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return -1;
    }

    sqlite3_stmt *stmt;
    view = (char*)malloc(3000);
    view[0] = '\0';
    ids = (int*)malloc(100 * sizeof(int));


    char* sql_query;
    if (page == -1) {
        sql_query = "select users.username, m1.message, m1.id, m2.message\
                    from messages as m1\
                    left join replies on m1.id = replies.child\
                    left join messages as m2 on replies.parent = m2.id\
                    join users on users.id = m1.sender\
                    where m1.receiver = ?1 and m1.read = 0;";
        sqlite3_prepare_v2(db, sql_query, -1, &stmt, NULL);
        sqlite3_bind_int(stmt, 1, id_user);
    }
    else {
        sql_query = "select users.username, m1.message, m1.id, m2.message\
                     from messages as m1\
                     left join replies on m1.id = replies.child\
                     left join messages as m2 on replies.parent = m2.id\
                     join users on users.id = m1.sender\
                     where m1.receiver = ?1\
                     order by m1.id\
                     limit ?2, 10;";
        sqlite3_prepare_v2(db, sql_query, -1, &stmt, NULL);
        sqlite3_bind_int(stmt, 1, id_user);
        sqlite3_bind_int(stmt, 2, page);
    }
    rc = sqlite3_step(stmt);

    int i = 0;
    while (rc == SQLITE_ROW)
    {
        const unsigned char* parent_msg = sqlite3_column_text(stmt, 3);
        if (parent_msg == NULL) {
            sprintf(view, "%s[%u] %s: %s\n",
                view, i + 1,
                (char*)sqlite3_column_text(stmt, 0),
                (char*)sqlite3_column_text(stmt, 1));
        }
        else {
            sprintf(view, "%s[%u] %s: %s\n (replied from your mesage: \"%s\")\n",
                view, i + 1,
                (char*)sqlite3_column_text(stmt, 0),
                (char*)sqlite3_column_text(stmt, 1),
                parent_msg);
        }
        ids[i] = sqlite3_column_int(stmt, 2);
        rc = sqlite3_step(stmt);
        ++i;
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
    return i;
}

