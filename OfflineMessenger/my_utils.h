// #include <sys/types.h>
// #include <sys/socket.h>
// #include <netinet/in.h>
// #include <errno.h>
// #include <unistd.h>
// #include <stdio.h>
// #include <stdlib.h>
// #include <netdb.h>
// #include <string.h>
// #include <arpa/inet.h>
#include <stdlib.h>



int read_integer(int sd, int& integer){
    int ret = read(sd, &integer, 4);
    integer = ntohl(integer);
    // printf("Am primit intregul |%d|\n", integer);
    return ret;
}

int write_integer(int sd, int integer){
    // printf("Trimit intregul |%d|\n", integer);
    integer = htonl(integer);
    int ret = write(sd, &integer, 4);
    return ret;
}

int read_string(int sd, char*& string){
    int len;
    int ret = read_integer(sd, len);
    if(ret < 0)
        return ret;
    string = (char*)malloc(len+1);
    int count = 0;
    while(count < len){
        ret = read(sd, string, len);
        if(ret < 0)
            return ret;
        count += ret;
    }
    string[len] = '\0';
    // printf("Am primit string-ul |%s|\n", string);
    return len;
}

int write_string(int sd, char* string){
    // printf("Trimit string-ul |%s|\n", string);
    int len = strlen(string);
    int ret = write_integer(sd, len);
    if(ret < 0)
        return ret;
    int count = 0;
    while(count < len){
        ret = write(sd, string, len);
        if(ret < 0)
            return ret;
        count += ret;
    }
    return len;
}