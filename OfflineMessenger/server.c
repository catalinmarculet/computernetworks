#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <malloc.h>
#include <sqlite3.h>
#include "my_utils.h"
#include "select.h"
#include "update.h"
#include "insert.h"

#define PORT 2020
#define clear() printf("\033[H\033[J")

#define DISPlAY_UNREAD 10
#define SEND_NEW 11
#define VIEW_HISTORY 12
#define REPLY 13
#define MARK_READ 14

extern int errno;

char *conv_addr(struct sockaddr_in address)
{
    static char str[25];
    char port[7];

    strcpy(str, inet_ntoa(address.sin_addr));
    bzero(port, 7);
    sprintf(port, ":%d", ntohs(address.sin_port));
    strcat(str, port);
    return (str);
}

int login(int fd);
int serve(int fd);

int client_ids[1000];

int main()
{
    clear();
    struct sockaddr_in server; 
    struct sockaddr_in from;
    fd_set readfds;
    fd_set actfds;     
    struct timeval tv; 
    int sd, client;   
    int optval = 1;
    int fd;
    int nfds;
    int len;

    if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("[server] Eroare la socket().\n");
        return errno;
    }

    setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    bzero(&server, sizeof(server));

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(PORT);

    if (bind(sd, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1)
    {
        perror("[server] Eroare la bind().\n");
        return errno;
    }

    if (listen(sd, 5) == -1)
    {
        perror("[server] Eroare la listen().\n");
        return errno;
    }

    /* completam multimea de descriptori de citire */
    FD_ZERO(&actfds);    /* initial, multimea este vida */
    FD_SET(sd, &actfds); /* includem in multime socketul creat */

    tv.tv_sec = 1; /* se va astepta un timp de 1 sec. */
    tv.tv_usec = 0;

    /* valoarea maxima a descriptorilor folositi */
    nfds = sd;

    printf("[server] Asteptam la portul %d...\n", PORT);
    fflush(stdout);

    for (int i = 0; i < 1000; i++)
        client_ids[i] = -1;
    /* servim in mod concurent clientii... */
    while (1)
    {
        /* ajustam multimea descriptorilor activi (efectiv utilizati) */
        bcopy((char *)&actfds, (char *)&readfds, sizeof(readfds));

        /* apelul select() */
        if (select(nfds + 1, &readfds, NULL, NULL, &tv) < 0)
        {
            perror("[server] Eroare la select().\n");
            return errno;
        }
        /* vedem daca e pregatit socketul pentru a-i accepta pe clienti */
        if (FD_ISSET(sd, &readfds))
        {
            /* pregatirea structurii client */
            len = sizeof(from);
            bzero(&from, sizeof(from));

            /* a venit un client, acceptam conexiunea */
            client = accept(sd, (struct sockaddr *)&from, (socklen_t*)&len);

            /* eroare la acceptarea conexiunii de la un client */
            if (client < 0)
            {
                perror("[server] Eroare la accept().\n");
                continue;
            }

            if (nfds < client) /* ajusteaza valoarea maximului */
                nfds = client;

            /* includem in lista de descriptori activi si acest socket */
            FD_SET(client, &actfds);

            printf("[server] S-a conectat clientul cu descriptorul %d, de la adresa %s.\n", client, conv_addr(from));
            fflush(stdout);
        }
        /* vedem daca e pregatit vreun socket client pentru a trimite raspunsul */

        
        for (fd = 0; fd <= nfds; fd++) 
        {
            if (fd != sd && FD_ISSET(fd, &readfds))
            {
                //Check if alive
                int alive;
                if(ioctl(fd, FIONREAD, &alive) == -1){
                    perror("Error at ioctl\n");
                    return 0;
                }
                if(!alive){
                    printf("[server] S-a deconectat clientul cu descriptorul %d.\n", fd);
                    fflush(stdout);
                    client_ids[fd] = -1;
                    close(fd);       
                    FD_CLR(fd, &actfds);
                    continue;
                }

                if (-1 == serve(fd)) {
                    return 0;
                }
            }
        } /* for */
    }     /* while */
} /* main */


int serve(int fd) {
    if (client_ids[fd] == -1) {
        client_ids[fd] = login(fd);
        printf("[server] Clientul %d a incercat sa se conecteze. Status: %d\n", fd, client_ids[fd]);
        write_integer(fd, client_ids[fd]);
        return 0;
    }

    int user_id = client_ids[fd];
    int option;
    int n = read_integer(fd, option);
    if (n < 0){
        perror("Error read() option\n");
        return -1;
    }

    printf("[server] Clientul cere optiunea %d.\n", option);
    if(option == DISPlAY_UNREAD){
        int* messages_ids;
        char* messages_view;
        int nr_messages = select_messages(user_id, messages_ids, messages_view, -1);

        write_integer(fd, nr_messages);
        for (int i = 0; i < nr_messages; i++)
            write_integer(fd, messages_ids[i]);
        write_string(fd, messages_view);
    }
    else if (option == MARK_READ) {
        printf("Marking as read for client %d\n", fd);
        mark_read(client_ids[fd]);
    }
    else if (option == REPLY) {
        int message_id;
        char* message;
        read_integer(fd, message_id);
        read_string(fd, message);
        insert_into_replies(message, message_id);
        printf("%s", message);
    }
    else if (option == VIEW_HISTORY) {
        int page;
        read_integer(fd, page);

        int* messages_ids;
        char* messages_view;
        int nr_messages = select_messages(user_id, messages_ids, messages_view, page);

        write_integer(fd, nr_messages);
        for (int i = 0; i < nr_messages; i++)
            write_integer(fd, messages_ids[i]);
        write_string(fd, messages_view);
    }
    else if (option == SEND_NEW) {
        char* username;
        read_string(fd, username);
        int receiver_id = select_user_id(username);
        if (receiver_id == -1) {
            write_integer(fd, -1);
            return 0;
        }
        write_integer(fd, 1);

        char* message;
        read_string(fd, message);
        insert_message(client_ids[fd], receiver_id, message);
    }
}


/* realizeaza primirea si retrimiterea unui mesaj unui client */

int login(int fd){
    char buffer[100];
    char* username;
    char* password;
    char response[100] = " ";
    int n;

    //Read username
    n = read_string(fd, username);
    if (n < 0)
    {
        perror("Error read() username\n");
        return 0;
    }

    //Read password
    n = read_string(fd, password);
    if (n < 0)
    {
        perror("Error read() password\n");
        return 0;
    }
/* este un socket de citire pregatit? */
    int id = select_user_id(username);
    if(id < 0){
        // bzero(response, 100);
        // strcat(response, "Username not found, please try again to login\n");
        // write_string(fd, response);
        return -1;
    }
    char* db_password = select_password(id);
    // printf("db_pass: %s\n", db_password);
    // printf("pass: %s\n", password);
    if(strcmp(password, db_password) != 0){
        // bzero(response, 100);
        // strcat(response, "Invalid password, please try again to login\n");
        // write_string(fd, response);
        return -1;
    }

    return id;
}
